﻿using AutoMapper;
using FileCirculation.Core.Constants;
using FileCirculation.Core.Models;
using FileCirculation.Repositories;
using FileCirculation.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileCirculation.ViewModels
{
    public class MainWindowViewModel : BaseViewModel
    {
        private readonly CaseRepository caseRepository;
        private readonly SupportingDocumentRepository supportingDocumentRepository;
        private readonly ActionItemRepository actionItemRepository;
        private readonly WindowService windowService;
        private readonly DocumentService documentService;
        private readonly IMapper mapper;
        private readonly User user;

        private IList<CaseViewModel> _cases;
        private CaseViewModel _selectedCase;
        private SupportingDocumentViewModel _selectedDocument;
        private IList<SupportingDocumentViewModel> _supportingDocuments;
        private ActionItemViewModel _selectedActionItem;
        private IList<ActionItemViewModel> _actionItems;
        private DateTime _currentCasesTime;

        public IList<CaseViewModel> Cases
        {
            get { return _cases; }
            set { SetValue(ref _cases, value); }
        }

        public CaseViewModel SelectedCase
        {
            get { return _selectedCase; }
            set { SetValue(ref _selectedCase, value); }
        }

        public SupportingDocumentViewModel SelectedDocument
        {
            get { return _selectedDocument; }
            set { SetValue(ref _selectedDocument, value); }
        }

        public IList<SupportingDocumentViewModel> SupportingDocuments
        {
            get { return _supportingDocuments; }
            set { SetValue(ref _supportingDocuments, value); }
        }

        public ActionItemViewModel SelectedActionItem
        {
            get { return _selectedActionItem; }
            set { SetValue(ref _selectedActionItem, value); }
        }

        public IList<ActionItemViewModel> ActionItems
        {
            get { return _actionItems; }
            set { SetValue(ref _actionItems, value); }
        }

        public DateTime CurrentCasesTime
        {
            get { return _currentCasesTime; }
            set { SetValue(ref _currentCasesTime, value); }
        }

        public string CurrentLoginDisplay
        {
            get
            {
                if (user == null)
                    return "Unknown user (Read-only access)";
                return $"{user.DisplayName} ({user.Department} {user.UserRole})";
            }
        }

        public bool IsUser
        {
            get
            {
                return (user == null) ? false : true;
            }
        }

        public MainWindowViewModel(User user, CaseRepository caseRepository, SupportingDocumentRepository supportingDocumentRepository, ActionItemRepository actionItemRepository, WindowService windowService, DocumentService documentService, IMapper mapper)
        {
            this.user = user;
            this.caseRepository = caseRepository;
            this.supportingDocumentRepository = supportingDocumentRepository;
            this.actionItemRepository = actionItemRepository;
            this.windowService = windowService;
            this.documentService = documentService;
            this.mapper = mapper;

            Cases = new ObservableCollection<CaseViewModel>();
            SupportingDocuments = new ObservableCollection<SupportingDocumentViewModel>();
            ActionItems = new ObservableCollection<ActionItemViewModel>();            

            RefreshCases();
            VerifyUser();
        }

        public async void RefreshCases()
        {
            var cases = await caseRepository.GetAllCases();

            Cases.Clear();
            foreach (Case item in cases)
                Cases.Add(mapper.Map<CaseViewModel>(item));

            CurrentCasesTime = DateTime.Now;

            if (SelectedCase == null)
                return;

            await RefreshSupportingDocuments();

            await RefreshActionItems();
        }

        private void VerifyUser()
        {
            if (user == null)
            {
                windowService.MessageBox("You are not a registered user of this program.", "User not found");
                //CurrentLogin = "Unknown user (Read-only access)";
                //IsUser = false;
                return;
            }

            //CurrentLogin = $"{user.DisplayName} ({user.Department} {user.UserRole})";
            //IsUser = true;
        }

        private async Task RefreshActionItems()
        {
            if (SelectedCase == null)
                return;

            var actionItems = await actionItemRepository.GetAllActionItems(SelectedCase.Id);
            ActionItems.Clear();
            foreach (ActionItem item in actionItems)
                ActionItems.Add(mapper.Map<ActionItemViewModel>(item));
        }

        private async Task RefreshSupportingDocuments()
        {
            if (SelectedCase == null)
                return;

            var supportingDocuments = await supportingDocumentRepository.GetAllDocuments(SelectedCase.Id);
            SupportingDocuments.Clear();
            foreach (SupportingDocument item in supportingDocuments)
                SupportingDocuments.Add(mapper.Map<SupportingDocumentViewModel>(item));
        }

        public async void OnCaseSelected(object selectedCase)
        {
            SelectedCase = selectedCase as CaseViewModel;

            await RefreshSupportingDocuments();
            SelectedDocument = null;

            await RefreshActionItems();
            SelectedActionItem = null;
        }

        public void OnActionItemSelected(object selectedActionItem)
        {
            SelectedActionItem = selectedActionItem as ActionItemViewModel;
        }

        public void TestButtonClicked()
        {
            windowService.MessageBox("", "");
        }

        public void LaunchCaseInitialisationWindow()
        {
            windowService.LaunchWindow(WindowService.WindowType.Initialisation, 0);
        }

        public void RemoveCase()
        {
            if (SelectedCase == null)
                return;

            caseRepository.RemoveCase(mapper.Map<Case>(SelectedCase));
            RefreshCases();
        }

        public void LaunchActionItemWindow()
        {
            if (!IsUser)
                return;

            if (SelectedActionItem == null)
                return;

            switch (SelectedActionItem.Title)
            {
                case ActionItemTitle.Initialise:
                    windowService.LaunchWindow(WindowService.WindowType.Initialisation, SelectedActionItem.Id);
                    break;
                case ActionItemTitle.ObtainDirectorApproval:
                case ActionItemTitle.ObtainMainCampusApproval:
                    windowService.LaunchWindow(WindowService.WindowType.Approval, SelectedActionItem.Id);
                    break;
                case ActionItemTitle.DepartmentalClosing:
                    windowService.LaunchWindow(WindowService.WindowType.DepartmentalClosing, SelectedActionItem.Id);
                    break;
            }
        }

        public void OnDocumentSelected(object item)
        {
            SelectedDocument = item as SupportingDocumentViewModel;
        }

        public void GoToDocument()
        {
            if (SelectedDocument == null)
                return;

            documentService.GoToDocument(SelectedDocument.Location);
        }
    }
}
