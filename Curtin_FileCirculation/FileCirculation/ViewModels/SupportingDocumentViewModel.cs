﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileCirculation.ViewModels
{
    public class SupportingDocumentViewModel : BaseViewModel
    {
        private int _id;
        public int Id
        {
            get { return _id; }
            set { SetValue(ref _id, value); }
        }

        private string _documentTitle;
        public string DocumentTitle
        {
            get { return _documentTitle; }
            set { SetValue(ref _documentTitle, value); }
        }

        private string _location;
        public string Location
        {
            get { return _location; }
            set { SetValue(ref _location, value); }
        }
    }
}
