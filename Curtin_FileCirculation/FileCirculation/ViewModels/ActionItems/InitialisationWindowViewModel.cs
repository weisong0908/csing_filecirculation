﻿using AutoMapper;
using FileCirculation.Core.Constants;
using FileCirculation.Core.Models;
using FileCirculation.Repositories;
using FileCirculation.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileCirculation.ViewModels.ActionItems
{
    public class InitialisationWindowViewModel : BaseViewModel
    {
        private readonly CaseRepository caseRepository;
        private readonly ActionItemRepository actionItemRepository;
        private readonly SupportingDocumentRepository supportingDocumentRepository;
        private readonly WindowService windowService;
        private readonly PdfService pdfService;
        private readonly DocumentService documentService;
        private readonly IMapper mapper;

        private int actionItemId;
        private CaseViewModel _case;
        private ActionItemViewModel _actionItem;
        private string enrolmentFormLocation;
        private IDictionary<string, string> formReadingResult;

        public CaseViewModel Case
        {
            get { return _case; }
            set { SetValue(ref _case, value); }
        }

        public ActionItemViewModel ActionItem
        {
            get { return _actionItem; }
            set { SetValue(ref _actionItem, value); }
        }

        public string Title { get; set; } = ActionItemTitle.Initialise;

        public IList<string> Purposes { get; set; }

        public bool CanInitialise
        {
            get
            {
                return (actionItemId == 0);
            }
        }

        public InitialisationWindowViewModel(int actionItemId, CaseRepository caseRepository, ActionItemRepository actionItemRepository, SupportingDocumentRepository supportingDocumentRepository, WindowService windowService, PdfService pdfService, DocumentService documentService, IMapper mapper)
        {
            this.caseRepository = caseRepository;
            this.actionItemRepository = actionItemRepository;
            this.supportingDocumentRepository = supportingDocumentRepository;
            this.windowService = windowService;
            this.pdfService = pdfService;
            this.documentService = documentService;
            this.mapper = mapper;

            Purposes = windowService.GetAllProperties(typeof(Purpose));

            this.actionItemId = actionItemId;
            SetUp();
        }

        public async void InitialiseCase()
        {
            Case.Progress = CaseProgress.Open;

            if (string.IsNullOrWhiteSpace(Case.Remarks))
                switch (Case.Purpose)
                {
                    case Purpose.Withdrawal:
                        Case.Remarks = "Withdrawal by: " + (string.IsNullOrWhiteSpace(formReadingResult[EnrolmentForm.WithdrawalStudyPeriod]) ? formReadingResult[EnrolmentForm.WithdrawalDate] : formReadingResult[EnrolmentForm.WithdrawalStudyPeriod]);
                        break;
                    case Purpose.ChangeOfMajor:
                        Case.Remarks = $"Change to {formReadingResult[EnrolmentForm.ChangeMajorTo]} by next trimester";
                        break;
                }

            caseRepository.AddCase(mapper.Map<Case>(Case));
            Case.Id = await caseRepository.GetCaseId(mapper.Map<Case>(Case));

            var actionItems = new List<ActionItem>();
            if (Case.Purpose == Purpose.Withdrawal)
            {
                actionItems.Add(CreateActionItem(ActionItemTitle.Initialise, Case.Id, ActionItemProgress.Closed));
                actionItems.Add(CreateActionItem(ActionItemTitle.ObtainDirectorApproval, Case.Id));
                actionItems.Add(CreateActionItem(ActionItemTitle.DepartmentalClosing, Case.Id));
            }

            if (Case.Purpose == Purpose.ChangeOfMajor)
            {
                actionItems.Add(CreateActionItem(ActionItemTitle.Initialise, Case.Id, ActionItemProgress.Closed));
                actionItems.Add(CreateActionItem(ActionItemTitle.ObtainMainCampusApproval, Case.Id));
                actionItems.Add(CreateActionItem(ActionItemTitle.DepartmentalClosing, Case.Id));
            }

            foreach (var actionItem in actionItems)
                actionItemRepository.AddActionItem(actionItem);

            var enrolmentForm = documentService.AddDocument(enrolmentFormLocation, mapper.Map<Case>(Case));

            supportingDocumentRepository.AddDocument(enrolmentForm);

            windowService.CloseWindow();
        }

        private ActionItem CreateActionItem(string actionItemTitle, int caseId, string progress = ActionItemProgress.Open)
        {
            return new ActionItem()
            {
                Department = Department.Academic,
                Title = actionItemTitle,
                Progress = progress,
                Remarks = string.Empty,
                CaseId = caseId
            };
        }

        public void ReadFromEnrolmentForm()
        {
            enrolmentFormLocation = windowService.OpenFileDialog();
            if (string.IsNullOrEmpty(enrolmentFormLocation))
                return;

            formReadingResult = pdfService.ReadEnrolmentForm(enrolmentFormLocation);

            Case.StudentId = formReadingResult[EnrolmentForm.StudentId];
            Case.StudentName = formReadingResult[EnrolmentForm.StudentName];
            Case.Course = $"{formReadingResult[EnrolmentForm.Course]} ({formReadingResult[EnrolmentForm.Major]})";
        }

        private async void SetUp()
        {
            if (actionItemId == 0)
                Case = new CaseViewModel()
                {
                    RequestDate = DateTime.Today
                };
            else
            {
                ActionItem = mapper.Map<ActionItemViewModel>(await actionItemRepository.GetActionItemById(actionItemId));
                Case = mapper.Map<CaseViewModel>(await caseRepository.GetCaseById(_actionItem.CaseId));
            }
        }
    }
}
