﻿using AutoMapper;
using FileCirculation.Core.Constants;
using FileCirculation.Core.Models;
using FileCirculation.Repositories;
using FileCirculation.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileCirculation.ViewModels.ActionItems
{
    public class DepartmentalClosingWindowViewModel : BaseViewModel
    {
        private int actionItemId;
        private CaseViewModel _case;
        private ActionItemViewModel _actionItem;
        private readonly CaseRepository caseRepository;
        private readonly ActionItemRepository actionItemRepository;
        private readonly IMapper mapper;
        private readonly WindowService windowService;
        private readonly EmailService emailService;

        public CaseViewModel Case
        {
            get { return _case; }
            set { SetValue(ref _case, value); }
        }
        public ActionItemViewModel ActionItem
        {
            get { return _actionItem; }
            set { SetValue(ref _actionItem, value); }
        }

        public ActionItemViewModel Approval { get; set; }

        public string Title { get; set; } = ActionItemTitle.DepartmentalClosing;

        public DepartmentalClosingWindowViewModel(int actionItemId, CaseRepository caseRepository, ActionItemRepository actionItemRepository, IMapper mapper, WindowService windowService, EmailService emailService)
        {
            this.actionItemId = actionItemId;
            this.caseRepository = caseRepository;
            this.actionItemRepository = actionItemRepository;
            this.mapper = mapper;
            this.windowService = windowService;
            this.emailService = emailService;

            SetUp();
        }

        private async void SetUp()
        {
            ActionItem = mapper.Map<ActionItemViewModel>(await actionItemRepository.GetActionItemById(actionItemId));
            Case = mapper.Map<CaseViewModel>(await caseRepository.GetCaseById(_actionItem.CaseId));
            var actionItems = await actionItemRepository.GetAllActionItems(Case.Id);
            Approval = mapper.Map<ActionItemViewModel>(actionItems.Where(ai => (ai.Title == ActionItemTitle.ObtainDirectorApproval || ai.Title == ActionItemTitle.ObtainMainCampusApproval) && ai.Id < actionItemId).Last());
        }

        public void SendEmail()
        {
            emailService.SendEmail(@"\\csing.navitas.local\shared\Documents\Quality Assurance\Personal WIP Folder - Wei Song\Projects\File circulation\ForDemo\CourseWithdrawal.oft", Case.Email);
        }

        public void CloseDepartment()
        {
            ActionItem.Progress = ActionItemProgress.Closed;
            actionItemRepository.UpdateProgress(mapper.Map<ActionItem>(ActionItem));
            windowService.CloseWindow();
        }
    }
}
