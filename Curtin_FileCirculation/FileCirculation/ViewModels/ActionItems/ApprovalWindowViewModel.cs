﻿using AutoMapper;
using FileCirculation.Core.Constants;
using FileCirculation.Core.Models;
using FileCirculation.Repositories;
using FileCirculation.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileCirculation.ViewModels.ActionItems
{
    public class ApprovalWindowViewModel : BaseViewModel
    {
        private int actionItemId;
        private readonly User user;
        private CaseViewModel _case;
        private ActionItemViewModel _actionItem;
        private SupportingDocumentViewModel _selectedDocument;
        private readonly CaseRepository caseRepository;
        private readonly ActionItemRepository actionItemRepository;
        private readonly SupportingDocumentRepository supportingDocumentRepository;
        private readonly IMapper mapper;
        private readonly WindowService windowService;
        private readonly DocumentService documentService;
        private IList<SupportingDocumentViewModel> _supportingDocument;

        public CaseViewModel Case
        {
            get { return _case; }
            set { SetValue(ref _case, value); }
        }

        public ActionItemViewModel ActionItem
        {
            get { return _actionItem; }
            set { SetValue(ref _actionItem, value); }
        }

        public SupportingDocumentViewModel SelectedDocument
        {
            get { return _selectedDocument; }
            set { SetValue(ref _selectedDocument, value); }
        }

        public IList<SupportingDocumentViewModel> SupportingDocuments
        {
            get { return _supportingDocument; }
            set { SetValue(ref _supportingDocument, value); }
        }

        public string Title { get { return ActionItem.Title; } }

        public bool CanGetApproval
        {
            get
            {
                if (ActionItem.Progress == ActionItemProgress.Closed)
                    return false;

                if (user.UserRole == UserRole.User)
                    return true;

                return false;
            }
        }

        public bool CanApproveOrReject
        {
            get
            {
                if (ActionItem.Progress == ActionItemProgress.Closed)
                    return false;

                switch (ActionItem.Title)
                {
                    case ActionItemTitle.ObtainDirectorApproval:
                        if (user.UserRole == UserRole.Director)
                            return true;
                        break;
                    case ActionItemTitle.ObtainMainCampusApproval:
                        return true;
                }

                return false;
            }
        }

        public ApprovalWindowViewModel(int actionItemId, User user, CaseRepository caseRepository, ActionItemRepository actionItemRepository, SupportingDocumentRepository supportingDocumentRepository, IMapper mapper, WindowService windowService, DocumentService documentService)
        {
            this.actionItemId = actionItemId;
            this.user = user;
            this.caseRepository = caseRepository;
            this.actionItemRepository = actionItemRepository;
            this.supportingDocumentRepository = supportingDocumentRepository;
            this.mapper = mapper;
            this.windowService = windowService;
            this.documentService = documentService;

            SupportingDocuments = new ObservableCollection<SupportingDocumentViewModel>();

            SetUp();
            GetSupportingDocuments();
        }

        public void AddDocument()
        {
            var documentPath = windowService.OpenFileDialog();

            if (string.IsNullOrEmpty(documentPath))
                return;

            var document = documentService.AddDocument(documentPath, mapper.Map<Case>(_case));

            supportingDocumentRepository.AddDocument(document);

            GetSupportingDocuments();
        }

        public void OnSupportingDocumentSelected(object selectedItem)
        {
            SelectedDocument = selectedItem as SupportingDocumentViewModel;
        }

        public void RemoveDocument()
        {
            var document = mapper.Map<SupportingDocument>(SelectedDocument);

            documentService.RemoveDocument(document);

            supportingDocumentRepository.RemoveDocument(document);

            GetSupportingDocuments();
        }

        public void GoToDocument()
        {
            if (SelectedDocument == null)
                return;

            documentService.GoToDocument(SelectedDocument.Location);
        }

        public void ApproveOrReject(bool IsApproved)
        {
            string remarks;
            if(IsApproved)
                remarks = $"Approved at {DateTime.Now.ToString()}";
            else
                remarks = $"Rejected at {DateTime.Now.ToString()}";

            ActionItem.Remarks = remarks;
            ActionItem.Progress = ActionItemProgress.Closed;
            actionItemRepository.UpdateProgress(mapper.Map<ActionItem>(ActionItem));
        }

        private async void SetUp()
        {
            ActionItem = mapper.Map<ActionItemViewModel>(await actionItemRepository.GetActionItemById(actionItemId));

            Case = mapper.Map<CaseViewModel>(await caseRepository.GetCaseById(_actionItem.CaseId));

            GetSupportingDocuments();
        }

        private async void GetSupportingDocuments()
        {
            var supportingDocuments = await supportingDocumentRepository.GetAllDocuments(_actionItem.CaseId);
            SupportingDocuments.Clear();
            foreach (var item in supportingDocuments)
                SupportingDocuments.Add(mapper.Map<SupportingDocumentViewModel>(item));
        }
    }
}
