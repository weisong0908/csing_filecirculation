﻿using FileCirculation.Core.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace FileCirculation.ViewModels
{
    public class CaseViewModel:BaseViewModel
    {
        private int _id;
        public int Id
        {
            get { return _id; }
            set { SetValue(ref _id, value); }
        }

        private string _studentId;
        public string StudentId
        {
            get { return _studentId; }
            set { SetValue(ref _studentId, value); }
        }
        
        private string _studentName;
        public string StudentName
        {
            get { return _studentName; }
            set { SetValue(ref _studentName, value); }
        }

        private string _email;
        public string Email
        {
            get { return _email; }
            set { SetValue(ref _email, value); }
        }

        private string _course;
        public string Course
        {
            get { return _course; }
            set { SetValue(ref _course, value); }
        }

        private string _purpose;
        public string Purpose
        {
            get { return _purpose; }
            set { SetValue(ref _purpose, value); }
        }

        private DateTime _requestDate;
        public DateTime RequestDate
        {
            get { return _requestDate; }
            set { SetValue(ref _requestDate, value); }
        }

        private string _progress;
        public string Progress
        {
            get { return _progress; }
            set { SetValue(ref _progress, value); }
        }      

        private string _remarks;
        public string Remarks
        {
            get { return _remarks; }
            set { SetValue(ref _remarks, value); }
        }
    }
}
