﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileCirculation.ViewModels
{
    public class ActionItemViewModel : BaseViewModel
    {
        private int _id;
        public int Id
        {
            get { return _id; }
            set { SetValue(ref _id, value); }
        }

        private string _department;
        public string Department
        {
            get { return _department; }
            set { SetValue(ref _department, value); }
        }

        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetValue(ref _title, value); }
        }

        private string _progress;
        public string Progress
        {
            get { return _progress; }
            set { SetValue(ref _progress, value); }
        }

        private string _remarks;
        public string Remarks
        {
            get { return _remarks; }
            set { SetValue(ref _remarks, value); }
        }

        private int _caseId;
        public int CaseId
        {
            get { return _caseId; }
            set { SetValue(ref _caseId, value); }
        }
    }
}
