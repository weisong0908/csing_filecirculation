﻿using FileCirculation.Views;
using FileCirculation.Views.ActionItems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Forms = System.Windows.Forms;

namespace FileCirculation.Services
{
    public class WindowService
    {
        public void MessageBox(string message, string caption)
        {
            System.Windows.MessageBox.Show(message, caption);
        }

        public void LaunchWindow(WindowType windowType, int? parameter = null)
        {
            Window window = null;

            switch(windowType)
            {
                case WindowType.Initialisation:
                    window = Application.Current.Windows.OfType<InitialisationWindow>().SingleOrDefault();
                    if (window == null)
                    {
                        window = new InitialisationWindow(parameter.Value);
                        window.ShowDialog();
                    }
                    break;
                case WindowType.Approval:
                    window = Application.Current.Windows.OfType<ApprovalWindow>().SingleOrDefault();
                    if (window == null)
                    {
                        window = new ApprovalWindow(parameter.Value);
                        window.ShowDialog();
                    }
                    break;
                case WindowType.DepartmentalClosing:
                    window = Application.Current.Windows.OfType<DepartmentalClosingWindow>().SingleOrDefault();
                    if (window == null)
                    {
                        window = new DepartmentalClosingWindow(parameter.Value);
                        window.ShowDialog();
                    }
                    break;
            }
        }

        public void CloseWindow()
        {
            Window window = Application.Current.Windows.OfType<Window>().SingleOrDefault(x => x.IsActive);
            window.Close();
        }

        public string OpenFileDialog()
        {
            var fileDialog = new Forms.OpenFileDialog();
            fileDialog.Filter = "PDF files (*.PDF)|*.PDF";

            if (fileDialog.ShowDialog() == Forms.DialogResult.OK)
                return fileDialog.FileName;

            return string.Empty;
        }

        public IList<string> GetAllProperties(Type type)
        {
            FieldInfo[] fields = type.GetFields();

            var result = new List<string>();

            foreach (FieldInfo field in fields)
                result.Add(field.GetValue(null).ToString());

            return result;
        }

        public enum WindowType
        {
            Initialisation,
            Approval,
            DepartmentalClosing
        }
    }
}
