﻿using FileCirculation.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;

namespace FileCirculation.Services
{
    public class DocumentService
    {
        private readonly string studentCentralisedDataFolderLocation;

        public DocumentService(string studentCentralisedDataFolderLocation)
        {
            this.studentCentralisedDataFolderLocation = studentCentralisedDataFolderLocation;
        }

        public SupportingDocument AddDocument(string documentPath, Case currentCase)
        {
            if (!File.Exists(documentPath))
                return null;

            var studentFolder = Path.Combine(studentCentralisedDataFolderLocation, currentCase.StudentId);
            var caseFolder = Path.Combine(studentFolder, $"Academic\\Course Variation\\{currentCase.Purpose}-{currentCase.RequestDate.Year}");
            
            if (!Directory.Exists(caseFolder))
                Directory.CreateDirectory(caseFolder);

            var destinationFile = Path.Combine(caseFolder, Path.GetFileName(documentPath));
            if (File.Exists(destinationFile))
                destinationFile = $"{caseFolder}\\{Path.GetFileNameWithoutExtension(documentPath)}-{DateTime.Now.Ticks}{Path.GetExtension(documentPath)}";
            File.Copy(documentPath, destinationFile);

            return new SupportingDocument()
            {
                DocumentTitle = Path.GetFileNameWithoutExtension(documentPath),
                Location = destinationFile,
                CaseId = currentCase.Id
            };
        }

        public void RemoveDocument(SupportingDocument document)
        {
            if (!File.Exists(document.Location))
                return;

            File.Delete(document.Location);
        }

        public void GoToDocument(string location)
        {
            if (!File.Exists(location))
                return;

            Process.Start(location);
        }
    }
}
