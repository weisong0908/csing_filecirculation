﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Outlook;

namespace FileCirculation.Services
{
    public class EmailService
    {
        private Application outlook;
        private MailItem mailItem;

        public void SendEmail(string templateLocation, string recipient)
        {
            outlook = new Application();
            mailItem = outlook.CreateItemFromTemplate(templateLocation) as MailItem;

            mailItem.To = recipient;

            mailItem.SendUsingAccount = outlook.Session.Accounts[1];

            mailItem.Display();
        }
    }
}
