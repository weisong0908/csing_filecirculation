﻿using FileCirculation.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iTextSharp.text.pdf;
using FileCirculation.Core.Constants;

namespace FileCirculation.Services
{
    public class PdfService
    {
        private PdfReader reader;

        public IDictionary<string, string> ReadEnrolmentForm(string fileName)
        {
            reader = new PdfReader(fileName);

            var result = new Dictionary<string, string>
            {
                { EnrolmentForm.StudentId, ReadField("studID") },
                { EnrolmentForm.StudentName, $"{ReadField("studGivennames")} {reader.AcroFields.GetField("studSurname")}" },
                { EnrolmentForm.Course, ReadField("studCourseTitle") },
                { EnrolmentForm.Major, ReadField("studMajorTitle") },
                { EnrolmentForm.WithdrawalStudyPeriod, $"{ReadField("Study Period")} {ReadField("Study year")}" },
                { EnrolmentForm.WithdrawalDate, GetDate(ReadField("DateWithdrawal")) },
                { EnrolmentForm.ChangeMajorTo, ReadField("Change Major") }
            };

            return result;
        }

        private string GetDate(string dateInString)
        {
            if (string.IsNullOrEmpty(dateInString))
                return dateInString;

            var day = int.Parse(dateInString.Substring(0, 2));
            var month = int.Parse(dateInString.Substring(2, 2));
            var year = int.Parse(dateInString.Substring(4, 4));

            var date = new DateTime(year, month, day);
            return date.ToShortDateString();
        }

        private string ReadField(string field)
        {
            string data = string.Empty;
            try { data = reader.AcroFields.GetField(field); }
            catch { }
            return data;
        }
    }
}
