﻿using AutoMapper;
using FileCirculation.ViewModels.ActionItems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FileCirculation.Views.ActionItems
{
    /// <summary>
    /// Interaction logic for InitialisationWindow.xaml
    /// </summary>
    public partial class InitialisationWindow : Window
    {
        public InitialisationWindowViewModel ViewModel
        {
            get { return DataContext as InitialisationWindowViewModel; }
            set { DataContext = value; }
        }

        public InitialisationWindow(int actionItemId = 0)
        {
            var thisApp = Application.Current as App;
            var caseRepository = thisApp.CaseRepository;
            var actionItemRepository = thisApp.ActionItemRepository;
            var supportingDocumentRepository = thisApp.SupportingDocumentRepository;
            var windowService = thisApp.WindowService;
            var pdfService = thisApp.PdfService;
            var documentService = thisApp.DocumentService;
            var mapper = Mapper.Instance;

            InitializeComponent();

            ViewModel = new InitialisationWindowViewModel(actionItemId, caseRepository, actionItemRepository, supportingDocumentRepository, windowService, pdfService, documentService, mapper);
        }

        private void ReadFromEnrolmentForm(object sender, RoutedEventArgs e)
        {
            ViewModel.ReadFromEnrolmentForm();
        }

        private void InitialiseCase(object sender, RoutedEventArgs e)
        {
            ViewModel.InitialiseCase();
        }
    }
}
