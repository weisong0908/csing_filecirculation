﻿using AutoMapper;
using FileCirculation.ViewModels.ActionItems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FileCirculation.Views.ActionItems
{
    /// <summary>
    /// Interaction logic for ApprovalWindow.xaml
    /// </summary>
    public partial class ApprovalWindow : Window
    {
        public ApprovalWindowViewModel ViewModel
        {
            get { return DataContext as ApprovalWindowViewModel; }
            set { DataContext = value; }
        }

        public ApprovalWindow(int actionItemId)
        {
            var thisApp = Application.Current as App;
            var caseRepository = thisApp.CaseRepository;
            var actionItemRepository = thisApp.ActionItemRepository;
            var supportingDocumentRepository = thisApp.SupportingDocumentRepository;
            var mapper = Mapper.Instance;
            var windowService = thisApp.WindowService;
            var documentService = thisApp.DocumentService;
            var user = thisApp.User;
        
            InitializeComponent();

            ViewModel = new ApprovalWindowViewModel(actionItemId, user, caseRepository, actionItemRepository, supportingDocumentRepository, mapper, windowService, documentService);
        }

        private void AddDocument(object sender, RoutedEventArgs e)
        {
            ViewModel.AddDocument();
        }

        private void RemoveDocument(object sender, RoutedEventArgs e)
        {
            ViewModel.RemoveDocument();
        }

        private void OnDocumentSelected(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 0)
                return;

            ViewModel.OnSupportingDocumentSelected(e.AddedItems[0]);
        }

        private void GoToDocument(object sender, RoutedEventArgs e)
        {
            ViewModel.GoToDocument();
        }

        private void Approve(object sender, RoutedEventArgs e)
        {
            ViewModel.ApproveOrReject(IsApproved: true);
        }

        private void Reject(object sender, RoutedEventArgs e)
        {
            ViewModel.ApproveOrReject(IsApproved: false);
        }
    }
}
