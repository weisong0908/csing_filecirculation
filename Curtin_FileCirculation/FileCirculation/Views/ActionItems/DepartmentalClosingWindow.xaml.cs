﻿using AutoMapper;
using FileCirculation.ViewModels.ActionItems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FileCirculation.Views.ActionItems
{
    /// <summary>
    /// Interaction logic for DepartmentalClosingWindow.xaml
    /// </summary>
    public partial class DepartmentalClosingWindow : Window
    {
        public DepartmentalClosingWindowViewModel ViewModel
        {
            get { return DataContext as DepartmentalClosingWindowViewModel; }
            set { DataContext = value; }
        }

        public DepartmentalClosingWindow(int actionItemId)
        {
            var thisApp = Application.Current as App;
            var caseRepository = thisApp.CaseRepository;
            var actionItemRepository = thisApp.ActionItemRepository;
            var mapper = Mapper.Instance;
            var windowService = thisApp.WindowService;
            var emailService = thisApp.EmailService;

            InitializeComponent();

            ViewModel = new DepartmentalClosingWindowViewModel(actionItemId, caseRepository, actionItemRepository, mapper, windowService, emailService);
        }

        private void SendEmail(object sender, RoutedEventArgs e)
        {
            ViewModel.SendEmail();
        }

        private void CloseDepartment(object sender, RoutedEventArgs e)
        {
            ViewModel.CloseDepartment();
        }
    }
}
