﻿using AutoMapper;
using FileCirculation.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FileCirculation.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindowViewModel ViewModel
        {
            get { return DataContext as MainWindowViewModel; }
            set { DataContext = value; }
        }

        public MainWindow()
        {
            InitializeComponent();

            var thisApp = Application.Current as App;
            var caseRepository = thisApp.CaseRepository;
            var supportingDocumentRepository = thisApp.SupportingDocumentRepository;
            var actionItemRepository = thisApp.ActionItemRepository;

            var mapper = Mapper.Instance;
            var windowService = thisApp.WindowService;
            var documentService = thisApp.DocumentService;

            var user = thisApp.User;

            ViewModel = new MainWindowViewModel(user, caseRepository, supportingDocumentRepository, actionItemRepository, windowService, documentService, mapper);
        }

        protected override void OnActivated(EventArgs e)
        {
            base.OnActivated(e);
            ViewModel.RefreshCases();
        }

        private void OnCaseSelected(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 0)
                return;

            ViewModel.OnCaseSelected(e.AddedItems[0]);
        }

        private void OnTestButtonClicked(object sender, RoutedEventArgs e)
        {
            ViewModel.TestButtonClicked();
        }

        private void OnActionItemSelected(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 0)
                return;

            ViewModel.OnActionItemSelected(e.AddedItems[0]);
        }

        private void Refresh(object sender, RoutedEventArgs e)
        {
            ViewModel.RefreshCases();
        }

        private void OpenActionItemWindow(object sender, RoutedEventArgs e)
        {
            ViewModel.LaunchActionItemWindow();
        }

        private void OpenCaseInitialisationWindow(object sender, RoutedEventArgs e)
        {
            ViewModel.LaunchCaseInitialisationWindow();
        }

        private void RemoveCase(object sender, RoutedEventArgs e)
        {
            ViewModel.RemoveCase();
        }

        private void GoToDocument(object sender, RoutedEventArgs e)
        {
            ViewModel.GoToDocument();
        }

        private void OnDocumentSelected(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 0)
                return;

            ViewModel.OnDocumentSelected(e.AddedItems[0]);
        }

        private void OnDoubleClickingSelectedActionItem(object sender, MouseButtonEventArgs e)
        {
            ViewModel.LaunchActionItemWindow();
        }
    }
}
