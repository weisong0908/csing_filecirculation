﻿using FileCirculation.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;
using FileCirculation.Core.Constants;

namespace FileCirculation.Repositories
{
    public class CaseRepository
    {
        private OleDbConnection connection;
        private OleDbDataReader dataReader;
        private OleDbCommand command;

        public CaseRepository(string connectionString)
        {
            connection = new OleDbConnection(connectionString);
        }

        public async Task<IEnumerable<Case>> GetAllCases()
        {
            await connection.OpenAsync();
            command = new OleDbCommand("SELECT * FROM Cases WHERE IsRemoved ='No'", connection);
            dataReader = command.ExecuteReader();

            var cases = new List<Case>();
            while (dataReader.Read())
            {
                cases.Add(new Case()
                {
                    Id = int.Parse(dataReader["Id"].ToString()),
                    StudentId = dataReader["StudentId"].ToString(),
                    StudentName = dataReader["StudentName"].ToString(),
                    Email = dataReader["Email"].ToString(),
                    Course = dataReader["Course"].ToString(),
                    Purpose = dataReader["Purpose"].ToString(),
                    RequestDate = DateTime.Parse(dataReader["StartDate"].ToString()),
                    Progress = dataReader["Progress"].ToString(),
                    Remarks = dataReader["Remarks"].ToString()
                });
            }

            dataReader.Close();
            connection.Close();

            return cases.OrderByDescending(c => c.Id).ToList();
        }

        public async void AddCase(Case newCase)
        {
            var sql = "INSERT INTO Cases " +
                "(StudentId, StudentName, Email, Course, Purpose, StartDate, Remarks, Progress, IsRemoved) " +
                "VALUES" +
                $"(@StudentId, @StudentName, @Email, @Course, @Purpose, @RequestDate, @Remarks, @Progress, @IsRemoved)";

            connection.Open();
            command = new OleDbCommand(sql, connection);

            command.Parameters.Add(new OleDbParameter("@StudentId", newCase.StudentId));
            command.Parameters.Add(new OleDbParameter("@StudentName", newCase.StudentName));
            command.Parameters.Add(new OleDbParameter("@Email", newCase.Email ?? string.Empty));
            command.Parameters.Add(new OleDbParameter("@Course", newCase.Course));
            command.Parameters.Add(new OleDbParameter("@Purpose", newCase.Purpose));
            command.Parameters.Add(new OleDbParameter("@RequestDate", newCase.RequestDate));
            command.Parameters.Add(new OleDbParameter("@Remarks", newCase.Remarks));
            command.Parameters.Add(new OleDbParameter("@Progress", newCase.Progress));
            command.Parameters.Add(new OleDbParameter("@IsRemoved", "No"));

            await command.ExecuteNonQueryAsync();
            connection.Close();
        }

        public async void RemoveCase(Case caseToBeRemoved)
        {
            var sql = $"UPDATE Cases SET IsRemoved = @IsRemoved WHERE Id = @Id";

            connection.Open();
            command = new OleDbCommand(sql, connection);

            command.Parameters.Add(new OleDbParameter("@IsRemoved", "Yes"));
            command.Parameters.Add(new OleDbParameter("@Id", caseToBeRemoved.Id));

            await command.ExecuteNonQueryAsync();
            connection.Close();
        }

        public async Task<int> GetCaseId(Case caseToLookUp)
        {
            var sql = $"SELECT TOP 1 Id FROM Cases WHERE StudentId = '{caseToLookUp.StudentId}' AND IsRemoved = 'No' ORDER BY Id DESC";
            await connection.OpenAsync();
            command = new OleDbCommand(sql, connection);
            dataReader = command.ExecuteReader();

            int id = 0;
            while (dataReader.Read())
            {
                id = int.Parse(dataReader[0].ToString());
            }

            dataReader.Close();
            connection.Close();

            return id;
        }

        public async Task<Case> GetCaseById(int caseId)
        {
            await connection.OpenAsync();
            command = new OleDbCommand($"SELECT * FROM Cases WHERE Id = {caseId}", connection);
            dataReader = command.ExecuteReader();

            var currentCase = new Case();
            while (dataReader.Read())
            {
                currentCase.Id = int.Parse(dataReader["Id"].ToString());
                currentCase.StudentId = dataReader["StudentId"].ToString();
                currentCase.StudentName = dataReader["StudentName"].ToString();
                currentCase.Email = dataReader["Email"].ToString();
                currentCase.Course = dataReader["Course"].ToString();
                currentCase.Purpose = dataReader["Purpose"].ToString();
                currentCase.RequestDate = DateTime.Parse(dataReader["StartDate"].ToString());
                currentCase.Progress = dataReader["Progress"].ToString();
                currentCase.Remarks = dataReader["Remarks"].ToString();
            };

            dataReader.Close();
            connection.Close();

            return currentCase;
        }
    }
}
