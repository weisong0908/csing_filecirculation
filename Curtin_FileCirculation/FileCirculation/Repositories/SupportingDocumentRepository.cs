﻿using FileCirculation.Core.Models;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileCirculation.Repositories
{
    public class SupportingDocumentRepository
    {
        private OleDbConnection connection;
        private OleDbDataReader dataReader;
        private OleDbCommand command;

        public SupportingDocumentRepository(string connectionString)
        {
            connection = new OleDbConnection(connectionString);
        }

        public async Task<IEnumerable<SupportingDocument>> GetAllDocuments(int caseId)
        {
            await connection.OpenAsync();
            command = new OleDbCommand($"SELECT * FROM SupportingDocuments WHERE CaseId = {caseId} AND IsRemoved ='No'", connection);
            dataReader = command.ExecuteReader();

            var supportingDocuments = new List<SupportingDocument>();
            while (dataReader.Read())
            {
                supportingDocuments.Add(new SupportingDocument()
                {
                    Id = int.Parse(dataReader["Id"].ToString()),
                    DocumentTitle = dataReader["DocumentTitle"].ToString(),
                    Location = dataReader["Location"].ToString(),
                    CaseId = int.Parse(dataReader["CaseId"].ToString())
                });
            }

            dataReader.Close();
            connection.Close();

            return supportingDocuments.OrderBy(sd => sd.Id).ToList();
        }

        public async void AddDocument(SupportingDocument document)
        {
            var sql = "INSERT INTO SupportingDocuments" +
                "(DocumentTitle, Location, CaseId, IsRemoved)" +
                "VALUES" +
                $"(@DocumentTitle, @Location, @CaseId, @IsRemoved)";

            connection.Open();
            command = new OleDbCommand(sql, connection);

            command.Parameters.Add(new OleDbParameter("@DocumentTitle", document.DocumentTitle));
            command.Parameters.Add(new OleDbParameter("@Location", document.Location));
            command.Parameters.Add(new OleDbParameter("@CaseId", document.CaseId));
            command.Parameters.Add(new OleDbParameter("@IsRemoved", "No"));

            await command.ExecuteNonQueryAsync();
            connection.Close();
        }

        public async void RemoveDocument(SupportingDocument document)
        {
            var sql = $"UPDATE SupportingDocuments SET IsRemoved = @IsRemoved WHERE Id = @Id";

            connection.Open();
            command = new OleDbCommand(sql, connection);

            command.Parameters.Add(new OleDbParameter("@IsRemoved", "Yes"));
            command.Parameters.Add(new OleDbParameter("@Id", document.Id));

            await command.ExecuteNonQueryAsync();
            connection.Close();
        }
    }
}
