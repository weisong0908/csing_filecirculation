﻿using FileCirculation.Core.Constants;
using FileCirculation.Core.Models;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileCirculation.Repositories
{
    public class ActionItemRepository
    {
        private OleDbConnection connection;
        private OleDbDataReader dataReader;
        private OleDbCommand command;

        public ActionItemRepository(string connectionString)
        {
            connection = new OleDbConnection(connectionString);
        }

        public async Task<IEnumerable<ActionItem>> GetAllActionItems(int caseId)
        {
            await connection.OpenAsync();
            command = new OleDbCommand($"SELECT * FROM ActionItems WHERE CaseId = {caseId} AND IsRemoved = 'No'", connection);
            dataReader = command.ExecuteReader();

            var actionItems = new List<ActionItem>();
            while (dataReader.Read())
            {
                actionItems.Add(new ActionItem()
                {
                    Id = int.Parse(dataReader["Id"].ToString()),
                    Department = dataReader["Department"].ToString(),
                    Title = dataReader["Title"].ToString(),
                    Progress = dataReader["Progress"].ToString(),
                    Remarks = dataReader["Remarks"].ToString(),
                    CaseId = int.Parse(dataReader["CaseId"].ToString())
                });
            }

            dataReader.Close();
            connection.Close();

            return actionItems.OrderBy(ai => ai.Id).ToList();
        }

        public async Task<ActionItem> GetActionItemById(int actionItemId)
        {
            await connection.OpenAsync();
            command = new OleDbCommand($"SELECT * FROM ActionItems WHERE Id = {actionItemId}", connection);
            dataReader = command.ExecuteReader();

            var actionItem = new ActionItem();
            while (dataReader.Read())
            {
                actionItem.Id = int.Parse(dataReader["Id"].ToString());
                actionItem.Department = dataReader["Department"].ToString();
                actionItem.Title = dataReader["Title"].ToString();
                actionItem.Progress = dataReader["Progress"].ToString();
                actionItem.Remarks = dataReader["Remarks"].ToString();
                actionItem.CaseId = int.Parse(dataReader["CaseId"].ToString());
            }

            dataReader.Close();
            connection.Close();

            return actionItem;
        }

        public async void AddActionItem(ActionItem actionItem)
        {
            var sql = "INSERT INTO ActionItems " +
                "(Department, Title, Progress, Remarks, CaseId, IsRemoved) " +
                "VALUES " +
                $"(@Department, @Title, @Progress, @Remarks, @CaseId, @IsRemoved)";

            connection.Open();
            command = new OleDbCommand(sql, connection);

            command.Parameters.Add(new OleDbParameter("@Department", actionItem.Department));
            command.Parameters.Add(new OleDbParameter("@Title", actionItem.Title));
            command.Parameters.Add(new OleDbParameter("@Progress", actionItem.Progress));
            command.Parameters.Add(new OleDbParameter("@Remarks", actionItem.Remarks));
            command.Parameters.Add(new OleDbParameter("@CaseId", actionItem.CaseId));
            command.Parameters.Add(new OleDbParameter("@IsRemoved", "No"));

            await command.ExecuteNonQueryAsync();
            connection.Close();
        }

        public async void UpdateProgress(ActionItem actionItem)
        {
            var sql = "UPDATE ActionItems SET " +
                "Progress = @Progress, " +
                "Remarks = @Remarks " +
                $"WHERE Id = {actionItem.Id}";

            connection.Open();
            command = new OleDbCommand(sql, connection);

            command.Parameters.Add(new OleDbParameter("@Progress", actionItem.Progress));
            command.Parameters.Add(new OleDbParameter("@Remarks", actionItem.Remarks));

            await command.ExecuteNonQueryAsync();
            connection.Close();
        }
    }
}