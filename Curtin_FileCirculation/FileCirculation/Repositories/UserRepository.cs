﻿using FileCirculation.Core.Models;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileCirculation.Repositories
{
    public class UserRepository
    {
        private OleDbConnection connection;
        private OleDbDataReader dataReader;
        private OleDbCommand command;

        public UserRepository(string connectionString)
        {
            connection = new OleDbConnection(connectionString);
        }

        public async Task<User> GetUserFromLogin(string loginUserName)
        {
            await connection.OpenAsync();
            command = new OleDbCommand($"SELECT * FROM Users", connection);
            dataReader = command.ExecuteReader();

            var users = new List<User>();
            while(dataReader.Read())
            {
                users.Add(new User()
                {
                    Id = int.Parse(dataReader["ID"].ToString()),
                    UserName = dataReader["UserName"].ToString(),
                    DisplayName = dataReader["DisplayName"].ToString(),
                    Email = dataReader["Email"].ToString(),
                    Department = dataReader["Department"].ToString(),
                    UserRole = dataReader["UserRole"].ToString()
                });
            }

            var user = users.Where(u => u.UserName.ToLower() == loginUserName.ToLower()).FirstOrDefault();

            dataReader.Close();
            connection.Close();

            return user;
        }
    }
}
