﻿using AutoMapper;
using FileCirculation.Core.Models;
using FileCirculation.ViewModels;

namespace FileCirculation.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Case, CaseViewModel>();
            CreateMap<SupportingDocument, SupportingDocumentViewModel>();
            CreateMap<ActionItem, ActionItemViewModel>();

            CreateMap<CaseViewModel, Case>();
            CreateMap<SupportingDocumentViewModel, SupportingDocument>();
            CreateMap<ActionItemViewModel, ActionItem>();
        }
    }
}