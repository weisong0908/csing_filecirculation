﻿using AutoMapper;
using FileCirculation.Core.Models;
using FileCirculation.Mapping;
using FileCirculation.Repositories;
using FileCirculation.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace FileCirculation
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private string connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source =\\csing.navitas.local\shared\Documents\Quality Assurance\Personal WIP Folder - Wei Song\Projects\File circulation\ForDemo\FileCirculationDatabase.mdb;Persist Security Info=True";
        private string studentCentralisedDataFolderLocation = @"\\csing.navitas.local\shared\Documents\Quality Assurance\Personal WIP Folder - Wei Song\Projects\File circulation\ForDemo\StudentCentralisedDataFolder";
        public CaseRepository CaseRepository { get; set; }
        public SupportingDocumentRepository SupportingDocumentRepository { get; set; }
        public ActionItemRepository ActionItemRepository { get; set; }
        public UserRepository UserRepository { get; set; }
        public WindowService WindowService { get; set; }
        public PdfService PdfService { get; set; }
        public DocumentService DocumentService { get; set; }
        public EmailService EmailService { get; set; }

        public User User { get; set; }

        protected override void OnStartup(StartupEventArgs e)
        {
            CaseRepository = new CaseRepository(connectionString);
            SupportingDocumentRepository = new SupportingDocumentRepository(connectionString);
            ActionItemRepository = new ActionItemRepository(connectionString);
            UserRepository = new UserRepository(connectionString);

            WindowService = new WindowService();
            PdfService = new PdfService();
            DocumentService = new DocumentService(studentCentralisedDataFolderLocation);
            EmailService = new EmailService();

            Mapper.Initialize(config => config.AddProfile<MappingProfile>());
            
            base.OnStartup(e);

            GetUser();

            Window mainWindow = new Views.MainWindow();
            mainWindow.Show();
        }

        private async void GetUser()
        {
            User = await UserRepository.GetUserFromLogin(Environment.UserName);
        }
    }
}
