﻿using System;
using System.Collections.Generic;

namespace FileCirculation.Core.Models
{
    public class Case
    {
        public int Id { get; set; }
        public string StudentId { get; set; }
        public string StudentName { get; set; }
        public string Email { get; set; }
        public string Course { get; set; }
        public string Purpose { get; set; }
        public DateTime RequestDate { get; set; }
        public string Progress { get; set; }
        public string Remarks;
        public IList<SupportingDocument> SupportingDocuments { get; set; }
        public IList<ActionItem> ActionItems { get; set; }


        public Case()
        {
            SupportingDocuments = new List<SupportingDocument>();
            ActionItems = new List<ActionItem>();
        }
    }
}
