﻿namespace FileCirculation.Core.Models
{
    public class ActionItem
    {
        public int Id { get; set; }
        public string Department { get; set; }
        public string Title { get; set; }
        public string Progress { get; set; }
        public string Remarks { get; set; }
        public int CaseId { get; set; }
    }
}
