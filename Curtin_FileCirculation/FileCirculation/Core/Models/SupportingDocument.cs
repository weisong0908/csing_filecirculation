﻿namespace FileCirculation.Core.Models
{
    public class SupportingDocument
    {
        public int Id { get; set; }
        public string DocumentTitle { get; set; }
        public string Location { get; set; }
        public int CaseId { get; set; }
    }
}
