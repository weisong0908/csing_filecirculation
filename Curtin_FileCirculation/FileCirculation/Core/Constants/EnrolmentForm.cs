﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileCirculation.Core.Constants
{
    public static class EnrolmentForm
    {
        public const string StudentId = "StudentId";
        public const string StudentName = "StudentName";
        public const string Course = "Course";
        public const string Major = "Major";
        public const string WithdrawalStudyPeriod = "WithdrawalStudyPeriod";
        public const string WithdrawalDate = "WithdrawalDate";
        public const string ChangeMajorTo = "ChangeMajorTo";
    }
}
