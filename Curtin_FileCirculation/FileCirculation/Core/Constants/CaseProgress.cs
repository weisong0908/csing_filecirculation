﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileCirculation.Core.Constants
{
    public static class CaseProgress
    {
        public const string Open = "Open";
        public const string Closed = "Closed";
    }
}
