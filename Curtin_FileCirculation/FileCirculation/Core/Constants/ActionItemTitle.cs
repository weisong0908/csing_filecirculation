﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileCirculation.Core.Constants
{
    public static class ActionItemTitle
    {
        public const string Initialise = "Initialise the case";
        public const string ObtainDirectorApproval = "Obtain director's approval";
        public const string ObtainMainCampusApproval = "Obtain approval from main campus";
        public const string InformStudentOutcome = "Inform the student outcome";
        public const string DepartmentalClosing = "Departmental closing";
    }
}