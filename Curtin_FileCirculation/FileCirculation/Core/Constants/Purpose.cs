﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileCirculation.Core.Constants
{
    public static class Purpose
    {
        public const string Withdrawal = "Withdrawal";
        public const string ChangeOfMajor = "Change of Major";
    }
}
